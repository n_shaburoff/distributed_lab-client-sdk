module gitlab.com/tokend/sdk

go 1.16

require (
	github.com/alecthomas/kingpin v2.2.6+incompatible
	github.com/btcsuite/btcd v0.21.0-beta
	github.com/btcsuite/btcutil v1.0.2
	github.com/certifi/gocertifi v0.0.0-20200922220541-2c3bb06c6054 // indirect
	github.com/edunuzzi/go-bip44 v0.0.0-20190109211530-eb6b7decf5cc // indirect
	github.com/ethereum/go-ethereum v1.10.2 // indirect
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible // indirect
	github.com/gobuffalo/packr/v2 v2.8.1
	github.com/google/jsonapi v1.0.0 // indirect
	github.com/nullstyle/go-xdr v0.0.0-20180726165426-f4c839f75077 // indirect
	github.com/robpike/filter v0.0.0-20150108201509-2984852a2183
	github.com/rubenv/sql-migrate v0.0.0-20210408115534-a32ed26c37ea
	github.com/tendermint/tendermint v0.34.9
	github.com/tyler-smith/go-bip39 v1.1.0 // indirect
	github.com/ubiq/go-ubiq v3.0.1+incompatible // indirect
	gitlab.com/distributed_lab/ape v1.5.0
	gitlab.com/distributed_lab/kit v1.8.5
	gitlab.com/distributed_lab/logan v3.8.0+incompatible
	gitlab.com/distributed_lab/lorem v0.2.0 // indirect
	gitlab.com/tokend/go v3.14.0+incompatible
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b
)
