package keypair

import (
	"errors"
)

var ErrInvalidSignature = errors.New("signature verification failed")

type KP interface {
	Address() string
	Verify(input []byte, signature []byte) error
	Sign(input []byte) ([]byte, error)
	ExportPublicKeyInPem() string
	ExportPrivateKeyInPem() string
}
