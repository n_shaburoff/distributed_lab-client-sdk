package keypair

import (
	"github.com/tendermint/tendermint/crypto/secp256k1"
	"gitlab.com/tokend/go/strkey"
	secp "gitlab.com/tokend/sdk/keypair/Secp256k1"
)

type Secp256k1 struct {
	seed string
}

func (kp *Secp256k1) Address() string {
	return strkey.MustEncode(strkey.VersionByteAccountID, kp.publicKey()[:])
}

func (kp *Secp256k1) Seed() string {
	return kp.seed
}

func (kp *Secp256k1) Verify(input []byte, sig []byte) error {
	if len(sig) != 64 {
		return ErrInvalidSignature
	}

	var asig [64]byte
	copy(asig[:], sig[:])

	pub, _ := kp.keys()

	if !pub.VerifySignature(input, asig[:]) {
		return ErrInvalidSignature
	}
	return nil
}

func (kp *Secp256k1) Sign(input []byte) ([]byte, error) {
	_, priv := kp.keys()
	return priv.Sign(input)
}

func (kp *Secp256k1) publicKey()  secp256k1.PubKey {
	pub, _ := kp.keys()
	return pub
}

func (kp *Secp256k1) keys() (publicKey secp256k1.PubKey, privateKey secp256k1.PrivKey) {
	priv := secp256k1.GenPrivKey()
	pub := priv.PubKey().Bytes()
	return pub, priv
}

func (kp *Secp256k1) ExportPublicKeyInPem() string {
	return string(secp.ParsePublicKey(secp.Secp256k1{}.PublicKey))
}

func (kp *Secp256k1) ExportPrivateKeyInPem() string {
	return string(secp.ParsePrivateKey(secp.Secp256k1{}.PrivateKey))
}


