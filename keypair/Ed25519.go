package keypair

import (
	"bytes"
	bs "encoding/base64"
	"golang.org/x/crypto/ed25519"

	"gitlab.com/tokend/go/strkey"
	"strings"
	ed "gitlab.com/tokend/sdk/keypair/Ed25519"
)

const ED25519_PEM_SECRET_KEY_TAG = "PRIVATE KEY";
const ED25519_PEM_PUBLIC_KEY_TAG = "PUBLIC KEY";

type Ed25519 struct {
	seed string
}

func (kp *Ed25519) Address() string {
	return strkey.MustEncode(strkey.VersionByteAccountID, kp.publicKey()[:])
}

func (kp *Ed25519) Verify(input []byte, sig []byte) error {
	if len(sig) != 64 {
		return ErrInvalidSignature
	}

	var asig [64]byte
	copy(asig[:], sig[:])

	if !ed25519.Verify(kp.publicKey(), input, asig[:]) {
		return ErrInvalidSignature
	}
	return nil
}

func (kp *Ed25519) Sign(input []byte) ([]byte, error) {
	_, priv := kp.keys()
	return ed25519.Sign(priv, input)[:], nil
}

func (kp *Ed25519) Seed() string {
	return kp.seed
}

func (kp *Ed25519) publicKey() ed25519.PublicKey {
	pub, _ := kp.keys()
	return pub
}

func (kp *Ed25519) keys() (publicKey ed25519.PublicKey, privateKey ed25519.PrivateKey) {
	reader := bytes.NewReader(kp.rawSeed())
	pub, priv, err := ed25519.GenerateKey(reader)
	if err != nil {
		panic(err)
	}
	return pub, priv
}

func (kp *Ed25519) rawSeed() []byte {
	return strkey.MustDecode(strkey.VersionByteSeed, kp.seed)
}

func (kp *Ed25519) ExportPrivateKeyInPem() string{
	derPrefix := make([]byte, 16)
	derPrefix[0] = 48;derPrefix[1] = 46;derPrefix[2] = 2;derPrefix[3] = 1
	derPrefix[4] = 0; derPrefix[5] = 48;derPrefix[6] = 5;derPrefix[7] = 6
	derPrefix[8] = 3; derPrefix[9] = 43;derPrefix[10] = 101;derPrefix[11] = 112
	derPrefix[12] = 4;derPrefix[13] = 34;derPrefix[14] = 4;derPrefix[15] = 32

	enc := append(derPrefix, ed.ParsePrivateKey(ed.Ed25519{}.PrivateKey)...)
	encoded, _ := bs.StdEncoding.DecodeString(string(enc))
	return kp.toPem(ED25519_PEM_SECRET_KEY_TAG, string(encoded))
}

func (kp *Ed25519) ExportPublicKeyInPem() string {
	derPrefix := make([]byte, 12)
	derPrefix[0] = 48;derPrefix[1] = 42;derPrefix[2] = 48;derPrefix[3] = 5
	derPrefix[4] = 6;derPrefix[5] = 3;derPrefix[6] = 43;derPrefix[7] = 101
	derPrefix[8] = 112;derPrefix[9] = 3;derPrefix[10] = 33;derPrefix[11] = 0

	enc := append(derPrefix, ed.Ed25519{}.PublicKey...)

	encoded, _ := bs.StdEncoding.DecodeString(string(enc))
	return kp.toPem(ED25519_PEM_PUBLIC_KEY_TAG, string(encoded))
}

func (kp *Ed25519) toPem(tag string, content string) string {
	s := "-----BEGIN ${tag}-----\n" + "{content}\n" + "-----END {tag}-----\n"
	res := pem(s, "{tag}", tag, "{content}",content)
	return res
}

func pem(format string, args ...string) string {
	r := strings.NewReplacer(args...)
	return r.Replace(format)
}