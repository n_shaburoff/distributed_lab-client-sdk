package keypair

import (
	"errors"
	"fmt"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcutil/hdkeychain"
	secp "gitlab.com/tokend/sdk/keypair/Secp256k1"
	"strings"
)

type HdKey interface {
	Sign(hash []byte) []byte
	Verify(hash []byte, signature []byte) error
}

type HDKey struct {
	HdKey 		hdkeychain.ExtendedKey
	Bip44Index  int
}


func NewHDKey(hdkey *hdkeychain.ExtendedKey) HDKey {
	return HDKey{*hdkey, 748}
}

func FromMasterSeed(seed []byte) HDKey {
	key, err := hdkeychain.NewMaster(seed, &chaincfg.MainNetParams)
	if err != nil {
		errors.New("can't generate a new master node using the seed")
	}
	return NewHDKey(key)
}

func (h HDKey) Derive(index int) secp.Secp256k1 {
	derivedChild, _ := h.HdKey.Child(uint32(index))
	privKey, _ := derivedChild.ECPrivKey()
	pubKey, _ := derivedChild.ECPubKey()
	return secp.Secp256k1{
		PublicKey: pubKey.SerializeCompressed(),
		PrivateKey: privKey.Serialize(),
	}
}

func Bip44Path(index int) string {
	key := HDKey{}
	path := []string {"m", "44'", fmt.Sprintf("%d", key.Bip44Index), "0'", "0",
						fmt.Sprintf("%d", index)}
	return strings.Join(path, "/")
}

func (h HDKey) DeriveIndex(inx int) secp.Secp256k1 {
	return h.Derive(Bip44Path(inx))
}

func (h HDKey) Sign(message []byte) []byte {
	sec := Secp256k1{}
	sig, _ := sec.Sign(message)
	return sig
}

func (h HDKey) Verify(msg []byte, sig []byte) error {
	sec := Secp256k1{}
	return sec.Verify(msg, sig)
}

func (h HDKey) PublicKey() []byte {
	return secp.Secp256k1{}.PublicKey
}

func (h HDKey) PrivateKey() []byte {
	return secp.Secp256k1{}.PrivateKey
}

