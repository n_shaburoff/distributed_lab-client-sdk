package keypair

type ToBytes interface {
	ToBytes() []byte
}

type PublicKey struct {
	RawPublicKey  []uint8
	Tag 		  int
}

func NewPublicKey(rawPublicKey []uint8, tag int) *PublicKey {
	p := new(PublicKey)
	p.RawPublicKey = rawPublicKey
	p.Tag = tag
	return p
}

func (p PublicKey) ToBytes() []uint8 {
	t := make([]uint8, 1)
	t[0] = uint8(p.Tag)
	rawBytes := ToBytesBytesArray(p.RawPublicKey)
	res := make([]uint8, 2)
	res = append(t, rawBytes...)
	return res
}

type Value struct {
	Bytes string
}