package Ed25519

import (
	bs "encoding/base64"
	"errors"
	"github.com/robpike/filter"
	"os"
	"strings"
)

type Ed25519 struct {
	PublicKey  []byte
	PrivateKey []byte
}

func ParseKey(bytes []byte, from int, to int) []byte {
	length := len(bytes)
	if key := bytes; length == 32 {
		return key
	} else if key := bytes[from:to]; length == 64 {
		return key
	} else if key := bytes[length %32:]; length > 32 && length < 64 {
		return key
	} else {
		return nil
	}
}

func ParsePrivateKey(bytes []byte) []byte{
	return ParseKey(bytes, 0, 32)
}

func ReadBase64File(path string) ([]byte,error) {
	content, err := os.ReadFile(path)
	if err != nil {
		return nil, errors.New("can't read file")
	}
	resContent := BytesToString(content)
	return ReadBase64WithPEM(resContent)
}

func BytesToString(data []byte) string {
	return string(data[:])
}

func ReadBase64WithPEM(content string) ([]byte, error) {
	base64 := strings.Split(content, "/\r\n/")
	var selec = filter.Choose(base64, filterFunction).([]string)
	join := strings.Join(selec, "")
	res := strings.Trim(join, "\r\n")
	return bs.StdEncoding.DecodeString(res)
}

func filterFunction(a string) bool {
	return ! strings.HasPrefix(a, "-----")
}

func ParsePrivateKeyFile(path string) []byte{
	read, _ := ReadBase64File(path)
	return ParsePrivateKey(read)
}

func ParsePublicKey(bytes []byte) []byte{
	return ParseKey(bytes, 32, 64)
}

func ParsePublicKeyFile(path string) []byte {
	read, _ := ReadBase64File(path)
	return ParsePublicKey(read)
}

func ParseKeyPair(publicKey []byte, privateKey []byte) *Ed25519 {
	pub := ParsePublicKey(publicKey)
	priv := ParsePrivateKey(privateKey)
	ed := Ed25519{
		PublicKey: pub,
		PrivateKey: append(priv, pub...),
	}
	return &ed
}

func ParseKeyFiles(publicKeyPath string, privateKeyPath string) *Ed25519 {
	publicKey := ParsePublicKeyFile(publicKeyPath)
	privateKey := ParsePrivateKeyFile(privateKeyPath)
	ed := Ed25519{
		PublicKey: publicKey,
		PrivateKey: append(privateKey, publicKey...),
	}
	return &ed
}

