package keypair

import "encoding/binary"

func ToBytesU64(number int) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(number))
	return b
}

func ToBytesString(str string) []byte {
	b := []byte(str)
	return b
}

func ToBytesU32(number int) []byte {
	b := make([]byte, 4)
	binary.LittleEndian.PutUint32(b, uint32(number))
	return b
}

func ToBytesArrayU8(arr []uint8) []uint8 {
	return append(ToBytesU32(len(arr)), arr...)
}