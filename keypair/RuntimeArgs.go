package keypair

type RuntimeArgs struct {
	Args map[string]Value
}

func NewRunTimeArgs(args map[string]Value) *RuntimeArgs {
	r := new(RuntimeArgs)
	r.Args = args
	return r
}

func (r RuntimeArgs) FromMap(args map[string]Value) *RuntimeArgs {
	m := make(map[string]Value)
	return NewRunTimeArgs(m)
}

func (r RuntimeArgs) Insert(key string, value Value) {
	r.Args[key] = value
}

func (r RuntimeArgs) ToBytes() []byte {

}