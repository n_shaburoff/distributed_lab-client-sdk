package Secp256k1

import (
	bs "encoding/base64"
	"encoding/pem"
	"errors"
	"github.com/robpike/filter"
	"os"
	"strings"
)

type Secp256k1 struct {
	PublicKey  []byte
	PrivateKey []byte
}

func ParsePrivateKey(bytes []byte) []byte{
	block := &pem.Block{
		Type: "PRIVATE KEY",
		Bytes: bytes,
	}
	priv := pem.EncodeToMemory(block)
	return priv
}

func ParsePublicKey(bytes []byte) []byte {
	block := &pem.Block{
		Type: "PUBLIC KEY",
		Bytes: bytes,
	}
	pub := pem.EncodeToMemory(block)
	return pub
}

func ReadBase64File(path string) ([]byte,error) {
	content, err := os.ReadFile(path)
	if err != nil {
		return nil, errors.New("can't read file")
	}
	resContent := BytesToString(content)
	return ReadBase64WithPEM(resContent)
}

func BytesToString(data []byte) string {
	return string(data[:])
}

func ReadBase64WithPEM(content string) ([]byte, error) {
	base64 := strings.Split(content, "/\r\n/")
	var selec = filter.Choose(base64, filterFunction).([]string)
	join := strings.Join(selec, "")
	res := strings.Trim(join, "\r\n")
	return bs.StdEncoding.DecodeString(res)
}

func filterFunction(a string) bool {
	return ! strings.HasPrefix(a, "-----")
}

func ParsePrivateKeyFile(path string) []byte {
	read, _ := ReadBase64File(path)
	return ParsePrivateKey(read)
}

func ParsePublicKeyFile(path string) []byte {
	read, _ := ReadBase64File(path)
	return ParsePublicKey(read)
}

func ParseKeyPair(publicKey []byte, privateKey []byte) *Secp256k1 {
	pub := ParsePublicKey(publicKey)
	priv := ParsePrivateKey(privateKey)
	secp := Secp256k1{
		PublicKey: pub,
		PrivateKey: priv,
	}
	return &secp
}


