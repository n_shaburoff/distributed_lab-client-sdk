package keypair

import (
	"golang.org/x/crypto/blake2b"
	"reflect"
	"time"
)

type DeployParams struct {
	AccountPublicKey	PublicKey
	ChainName 			string
	GasPrice 			int
	Ttl 				int
	Dependencies		[]uint8
	Timestamp			int
}

func NewDeployParams(accountPublicKey PublicKey, chainName string, dependencies []uint8, timestamp int) *DeployParams {
	d := new(DeployParams)
	d.AccountPublicKey = accountPublicKey
	d.ChainName = chainName
	d.GasPrice = 1
	d.Ttl = 1800000
	d.Dependencies = []uint8{}
	if timestamp == 0 {
		d.Timestamp = int(time.Now().Unix())
	}
	return d
}

func MakeDeploy(deployParam DeployParams, session ExecutableDeployItem, payment ExecutableDeployItem) Deploy {
	serializedBody := SerializeBody(payment, session)
	bodyHash, _ := blake2b.New256(serializedBody)
	resBodyHash := make([]uint8, bodyHash.Size())
	bodyHash.Sum(resBodyHash)

	header := NewDeployHeader(
		deployParam.AccountPublicKey,
		deployParam.Timestamp,
		deployParam.Ttl,
		deployParam.GasPrice,
		resBodyHash,
		deployParam.Dependencies,
		deployParam.ChainName,
		)

	serializedHeader := SerializeHeader(*header)
	deployHash, _ :=blake2b.New256(serializedHeader)
	resDeployHash := make([]uint8, deployHash.Size())
	deployHash.Sum(resDeployHash)
	var approvals []Approval
	return *NewDeploy(resDeployHash, *header, payment, session, approvals)
}

type Deploy struct {
	Hash []uint8
	Header DeployHeader
	Payment ExecutableDeployItem
	Session ExecutableDeployItem
	Approvals []Approval
}

func NewDeploy(hash []uint8, header DeployHeader, payment ExecutableDeployItem, sessions ExecutableDeployItem,
	           approvals []Approval) *Deploy{
	d := new(Deploy)
	d.Hash = hash
	d.Header = header
	d.Payment = payment
	d.Session = sessions
	d.Approvals = approvals
	return d
}

type Approval struct {
	Signature string
	Signer    string
}

type DeployHeader struct {
	Account		 PublicKey
	BodyHash 	 []uint8
	ChainName 	 string
	Dependencies []uint8
	GasPrice 	 int
	TimeStamp 	 int
	Ttl 		 int
}

func NewDeployHeader(account PublicKey, timeStamp int, ttl int , gasPrice int,
	                 bodyHash []uint8, dependencies []uint8, chainName string) *DeployHeader{
	d := new(DeployHeader)
	d.Account = account
	d.TimeStamp = timeStamp
	d.Ttl = ttl
	d.GasPrice = gasPrice
	d.BodyHash = bodyHash
	d.Dependencies = dependencies
	d.ChainName = chainName

	return d
}

func (d DeployHeader) ToBytes() []uint8 {
	acc := d.Account.ToBytes()
	timeSt := ToBytesU64(d.TimeStamp)
	result := append(acc, timeSt...)
	ttl := ToBytesU64(d.Ttl)
	result = append(result, ttl...)
	gasPr := ToBytesU64(d.GasPrice)
	result = append(result, gasPr...)
	bodyH := ToBytesBytesArray(d.BodyHash)
	result = append(result, bodyH...)
	chainN := ToBytesString(d.ChainName)
	result = append(result, chainN...)
	return result
}

func ToBytesBytesArray(arr []uint8) []uint8 {
	return arr
}

type ExecutableDeployItem struct {
	ModuleBytes ModuleBytes
	Transfer 	Transfer
}

func (e ExecutableDeployItem) IsModuleBytes() bool {
	if reflect.TypeOf(e).Name() == "ModuleBytes" {
		return true
	} else {
		return false
	}

}

func (e ExecutableDeployItem) IsTransfer() bool {
	if reflect.TypeOf(e).Name() == "Transfer" {
		return true
	} else {
		return false
	}
}

func (e ExecutableDeployItem) ToBytes() []byte {
	if e.IsModuleBytes() {
		return e.ModuleBytes.ToBytes()
	} else if e.IsTransfer() {
		return e.Transfer.ToBytes()
	} else {
		return []byte{}
	}
}

func (e ExecutableDeployItem) NewTransfer(amount int, target PublicKey, id int) *Transfer {
	r := RuntimeArgs{}
	val := Value{}
	mAp := make(map[string]Value)
	runtimeArgs := r.FromMap(mAp)
	runtimeArgs.Insert("amount", val)
	if reflect.TypeOf(target).Name() != "PublicKey" {
		runtimeArgs.Insert("source", val)
	}
	runtimeArgs.Insert("id", val)
	return NewTransfer(*runtimeArgs)
}

type Transfer struct {
	Tag 	int
	Args 	RuntimeArgs
}

func NewTransfer(args RuntimeArgs) *Transfer {
	return &Transfer{5, args}
}

func (t Transfer) ToBytes() []byte {
	a := make([]uint8, 1)
	a[0] = uint8(t.Tag)
	return append(a, ToBytesBytesArray(t.Args.ToBytes())...)
}

func SerializeBody(payment ExecutableDeployItem, session ExecutableDeployItem) []byte {
	return append(payment.ToBytes(), session.ToBytes()...)

}

func SerializeHeader(depHead DeployHeader) []uint8 {
	return depHead.ToBytes()
}

type ModuleBytes struct {
	Tag			int
	ModlBytes 	[]uint8
	Args 		RuntimeArgs
}

func newModuleBytes(moduleBytes []uint8, args RuntimeArgs) ModuleBytes {
	return ModuleBytes{
		ModlBytes: moduleBytes,
		Args: args,
	}
}

func (m ModuleBytes) ToBytes() []byte {
	res := []byte{}
	res = append(ToBytesU32(m.Tag), ToBytesArrayU8(m.ModlBytes)...)
	res = append(res, ToBytesBytesArray(m.Args.ToBytes())...)
	return res
}