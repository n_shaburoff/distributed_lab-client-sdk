FROM golang:1.12

WORKDIR /go/src/gitlab.com/tokend/sdk

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/sdk gitlab.com/tokend/sdk


###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/sdk /usr/local/bin/sdk
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["sdk"]
